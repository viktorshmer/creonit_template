;(function(){
	
  var request = new XMLHttpRequest(),
  	listProduct = document.querySelector('.list-product'),
  	countProductBasket = document.querySelector('.basket__count'),
  	priceProductBasket = document.querySelector('.basket__price');

// ПОЛУЧЕНИЕ ТОВАРОВ В КОРЗИНЕ
  function getBasket() {
  	request.open('GET', 'http://vue-tests.dev.creonit.ru/api/cart/list', true);
  	request.onload = function () {
  		if (request.status == 200) {
  			var itemsBasket = JSON.parse(request.response),
  				countItemsBasket = itemsBasket.amount,
  				priceItemsBasket = itemsBasket.price;
  			countProductBasket.innerText = countItemsBasket;
  			priceProductBasket.innerText = priceItemsBasket;
  		}
  	}
  	

  	request.send();
  	
  }

// ЗАПРОС К API
  function getAPI($method = 'GET', $url = 'catalog/shiny', $getCatalog = true) {

  request.open($method, 'http://vue-tests.dev.creonit.ru/api/'+$url, true);
  request.onload = function () {
  	if (request.status == 200) {
  		( $getCatalog ) ? getCatalog(request) : addCart();
  	}
  	else {
  		alert('Ошибочка');
  	}
  }
  request.send();

  }

// ПОЛУЧЕНИЕ КАТАЛОГА
  function getCatalog(request) {
  	var terms = JSON.parse(request.response),
  	$items = terms.items,
  	count = $items.length,
  	listLi = '';
  	// console.log(terms);
  	for (var i = 0; i < count; i++) {
  		listLi += `<li>
  						<img src='${$items[i].image_preview}' alt='${$items[i].title}' />
  						<span class='name-product'>${$items[i].title} - ${$items[i].price} руб.</span>
  						<button class='buy' data-id='${$items[i].id}'>В корзину</button>
  					</li>`;
  	};
  	listProduct.innerHTML = listLi;
  	
  }

// ДОАВЛЕНИЕ В КОРЗИНУ
  function addCart() {
  alert('Товар добавлен в корзину');
  getBasket(); 	

  }

// ОБРАБОТЧИК КЛИКОВ
  document.addEventListener('click', function(e){
  	var $target = e.target;

  	if($target.classList[1] == 'get-catalog') {
  		var catalog = $target.dataset.type,
    		$url = 'catalog/'+catalog,
    		$method = 'GET';
  		getAPI($method, $url);
  	}
  	else if ($target.classList[0] == 'buy') {
  		var id = $target.dataset.id,
    		$method = 'POST',
    		$url = 'cart/product/'+id,
    		$getCatalog = false;
  		getAPI($method, $url, $getCatalog);
  	}
  	
  });


  getAPI();
  setTimeout(function(){
  	getBasket();
  },500);


})();