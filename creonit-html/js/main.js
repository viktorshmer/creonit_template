window.onload = function(){
	/*
	var btns = document.querySelectorAll('.buttons-view__btn'),
		catalogNewsWrap = document.querySelector('.catalog-news__wr');
	function clickBtns(e){
		e.preventDefault();
		switch (this.dataset.view) {
			case 'thumb': 
				this.classList.add('active');
				catalogNewsWrap.classList.remove('list');
				catalogNewsWrap.classList.add(this.dataset.view);
				this.nextElementSibling.classList.remove('active');
				break;
			case 'list':
				this.classList.add('active');
				catalogNewsWrap.classList.remove('thumb');
				catalogNewsWrap.classList.add(this.dataset.view);
				this.previousElementSibling.classList.remove('active');
				break;
		}
	}
	btns[0].addEventListener('click', clickBtns);
	btns[1].addEventListener('click', clickBtns);
*/
	document.addEventListener('click', function(e){
		var $this = e.target;
		if ($this.classList.contains('buttons-view__btn') && !$this.classList.contains('active')) {
						
			($this.previousElementSibling) ? $this.previousElementSibling.classList.remove('active') : null;
			($this.nextElementSibling) ? $this.nextElementSibling.classList.remove('active') : null; 
			$this.className += ' active';

			var catalogNewsWrap = document.querySelector('.catalog-news__wr');
			catalogNewsWrap.classList.remove('thumb');
			catalogNewsWrap.classList.remove('list');
			catalogNewsWrap.className += ' '+$this.dataset.view;
		}
	});

}